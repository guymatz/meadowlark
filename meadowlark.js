var express = require('express');
var app = express();
var handlebars = require('express-handlebars')
	.create({ defaultLayout:'main'} );
var fortune = require('./lib/fortune.js');

app.engine('handlebars', handlebars.engine );
app.set('view engine', 'handlebars');

app.set('port', process.env.port || 3000);

app.use(express.static(__dirname + '/public'));

app.use(function(req, res, next) {
        res.locals.showTests = app.get('env') !== 'production' &&
                req.query.test === '1';
        next();
});

app.get('/', function(req, res) {
	res.render('home');
});

app.get('/about', function(req, res) {
        var f = fortune.getFortune() 
        // console.log("F = " + f);
	res.render('about', { fortune: f });
});

app.use(function(req, res) {
	res.status(404);
	res.render('404');
});

app.use(function(req, res, next) {
	console.error(err.stack);
	res.status(500);
	res.render('500');
});

app.listen(app.get('port'), function() {
	console.log('Express started on http://localhost:' +
	app.get('port') + '; press ctrl-c to terminate.');
});
